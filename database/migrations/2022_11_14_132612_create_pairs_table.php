<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->id();

            $table->integer('owner_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('like')->default(false);
            $table->integer('dislike')->default(false);
            $table->boolean('match')->default(false);

            $table->foreign('owner_id')->nullOnDelete()->references('id')->on('users');
            $table->foreign('user_id')->nullOnDelete()->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairs');
    }
};
