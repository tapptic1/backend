<?php

namespace App\Http\Controllers;

use App\Http\Requests\SwipeRequest;
use App\Models\Pair;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class SwipeController extends Controller
{
    public function swipe(SwipeRequest $request)
    {
        $validated = $request->validated();

        if ($validated) {
            $owner = User::where('id', '=', $request->get('owner'))->first();
            $user = User::where('id', '=', $request->get('user'))->first();
            $action = $request->get('action');

            if (!$owner) {
                return response()->json([
                    'data' => [
                        'success' => 'Not found owner!'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            if (!$user) {
                return response()->json([
                    'data' => [
                        'success' => 'Not found user!'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            if (
                $action == 'like' ||
                $action == 'dislike'
            ) {
                $ownerPair = Pair::where('owner_id', '=', $owner->id)->where('user_id', '=', $user->id)->first();
                $userPair = Pair::where('owner_id', '=', $user->id)->where('user_id', '=', $owner->id)->first();

                if (
                    !$ownerPair &&
                    !$userPair
                ) {
                    $pair = new Pair();

                    $pair->owner()->associate($owner);
                    $pair->user()->associate($user);

                    $pair->like = $action == 'like' ? 1 : 0;
                    $pair->dislike = $action == 'dislike'? 1 : 0;

                    $pair->save();
                }

                if (
                    $userPair
                ) {
                    $userPair->like = $action == 'like' ? $userPair->like + 1 : $userPair->like;
                    $userPair->dislike = $action == 'dislike' ? $userPair->dislike + 1 : $userPair->dislike;
                    $userPair->match = $userPair->like === 2;

                    if ($userPair->match) {
                        $userPair->like = 2;
                        $userPair->dislike = 0;
                    }

                    $userPair->save();
                }
            } else {
                return response()->json([
                    'data' => [
                        'success' => 'Action is could be like or dislike!'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            return response()->json([
                'data' => [
                    'success' => 'Success.'
                ]
            ], Response::HTTP_OK);
        }
    }
}
